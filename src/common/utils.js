//获取px与rpx之间的比列
function getRpx(px) {
    let winWidth = uni.getSystemInfoSync().windowWidth;
    let scale = 750 / winWidth;
    return parseInt(px * scale);
}

function getPx(rpx) {
    let winWidth = uni.getSystemInfoSync().windowWidth;
    let scale = 750 / winWidth;
    return parseInt(rpx / scale);
}

export function pictureResolution(h = 100, w = 100, mode = 3) {
    let width = Math.round(getPx(w) * 1.2);
    let height = Math.round(getPx(h) * 1.2);


    if (mode === 1) {
        return `?x-oss-process=image/resize,w_${width},h_${height},limit_0`
    }

    if (mode === 2) {
        return `?x-oss-process=image/resize,h_${height},limit_0`
    }

    if (mode === 3) {
        return `?x-oss-process=image/resize,w_${width},limit_0/format,webp`
    }
}

function parseScene(scene) {
    let params = {}
    scene = decodeURIComponent(scene).split('&');
    for (let i = 0; i < scene.length; i++) {
        params[scene[i].split('=')[0]] = scene[i].split('=')[1];
    }
    return params
}

function getLocation(fn) {
    uni.getSetting({
        success: (res) => {
            res.authSetting
            if (res.authSetting["scope.userLocation"] === undefined) {
                uni.authorize({
                    scope: "scope.userLocation",
                    success: () => {
                        fn()
                    }
                })
            }

            if (res.authSetting["scope.userLocation"] === false) {
                toast("自动定位需要获得定位权限")
                uni.openSetting({
                    success: (res) => {
                        if (res.authSetting["scope.userLocation"]) {
                            fn()
                        }
                    }
                })
            }

            if (!!res.authSetting["scope.userLocation"]) {
                fn()
            }
        }
    })
}
