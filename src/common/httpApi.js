/**
 * API接口配置文件
 */
module.exports = {
    enums: {
        getPanelsInfo: "enum/getPanelsInfo",//板块类型
        getUrlType: "enum/getUrlType",//链接跳转类型
    },
    home: {
        banner: "https://mobo-peafowls.cn/project/store/banner", // 首页banner
        category: "home/Category?parentId=0", // 获取商品分类
        withChildren: "home/Category/withChildren", // 查询所有一级分类及子分类
        categoryid: "home/Category/{id}", //根据id获取商品分类
        menus: "home/menus", // 首页菜单项
        panels: "home/panels", // 在菜单中的板块项
        freeProductList: "home/recommendProductList", // 分页获取推荐商品-上拉刷新
        recommendProductList: "home/recommendProductList", // 分页获取推荐商品-上拉刷新
        subjectList: "home/subjectList", //根据分类获取专题
    },
    member: {
        addBrand: "member/attention/add", // 添加品牌关注
        deleteBrand: "member/attention/delete", // 取消品牌关注
        listBrand: "member/attention/list/{memberId}", // 显示关注品牌列表
        addProduct: "member/collection/addProduct", // 添加商品收藏
        deleteProduct: "member/collection/deleteProduct", // 删除商品收藏
        listProduct: "member/collection/listProduct/", // 显示商品收藏列表
        createHistory: "member/readHistory/create", // 创建浏览记录
        deleteHistory: "member/readHistory/delete", // 删除浏览记录
        listHistory: "member/readHistory/list", // 展示浏览记录
        addAddress: "member/address/add", // 添加收货地址
        deleteAddress: "member/address/delete/", // 删除收货地址
        listAddress: "member/address/list", // 显示所有收货地址
        updateAddress: "member/address/update/", // 修改收货地址
        detailAddress: "member/address/{id}", // 通过ID获取收货地址
    },
    cart: {
        addProductId: "cart/add/product/", // 添加商品(productId)到购物车
        addSkuCode: "cart/add/sku/", // 添加商品(skuCode)到购物车
        clearCart: "cart/clear", // 清空购物车
        deleteCart: "cart/delete", // 删除购物车中的某个商品
        listCart: "cart/list", //获取某个会员的购物车列表
        promotion: "cart/list/promotion", // 获取某个会员的购物车列表,包括促销信息
        updateQuantity: "cart/update/quantity", // 修改购物车中某个商品的数量
        select: "cart/update/select/{status}", // 修改购物车中商品选中状态
    },
    order: {
        cancelOrder: "order/cancelOrder", // 取消单个超时订单
        cancelTimeOutOrder: "order/cancelTimeOutOrder", // 自动取消超时订单
        generateConfirmOrder: "order/generateConfirmOrder", //根据购物车信息生成确认单信息
        generateFlashConfirmOrder: "order/generateFlashConfirmOrder", // 立即购买生成确认单信息
        generateFlashOrder: "order/generateFlashOrder",//闪购生成订单
        generateFlashOrderSimple: "order/generateFlashOrderSimple",//闪购生成订单
        generateOrder: "order/generateOrder", // 根据购物车信息生成订单
        list: "order/list", // 查询订单
        listByStatus: "order/listByStatus",
        notify: "order/notify", // 支付成功的回调
        detail: "order/OrderDetail/",//获取订单详情:订单信息、商品信息、操作记录
        refirm: "order/confirm_order/",//确认收货
        repay: "order/rePayOrder/",//重新支付
    },
    returnApply: {
        create: "returnApply/create",//申请退货
        list: "returnApply/list",
        getDetail: "returnApply/getDetail",
    },
    logistics: {
        detail: "logistics/detail/",//物流查看
        delivery: "logistics/detailByDelivery_sn/",//物流查询
    },
    product: {
        list: "product/list",//查询商品
        shop: "product/previewShop",//根据商铺编号查询店铺信息
        simpleList: "product/simpleList",//根据商品名称或货号模糊查询
        detail: "product/",//根据商品id获取商品详细信息
        applyFreeProduct: "product/applyFreeProduct",//申请免费商品
        applyFreeProductSimple: "product/applyFreeProductSimple",//直接申请免费商品
        getMyFreeProductList: "product/getMyFreeProductList",//查询我已经领取的免费商品
        getProductInfo: "product/getProductInfo", //根据商品id和店铺ID获取商品详细信息
        listProductWithDistance: "product/listProductWithDistance",//根据距离获取商品详细信息
        getQueryDict: "product/getQueryDict",//根据距离获取商品详细信息
    },
    shop: {
        getShopDetail: "shop/getShopDetail",//根据shopid查询商品主页内容
    },
    coupon: {
        list: "coupon/list",//根据优惠券id，使用状态，订单编号分页获取会员下的已有的优惠券
        cart: "coupon/list/cart/{type}",//获取登录会员购物车的相关优惠券
        product: "coupon/product/{id}",//获取商品可用的优惠券
        receive: "coupon/receive/{couponId}",//领取指定优惠券
        detail: "coupon/{id}",//获取单个优惠券的详细信息
    },
    sso: {
        bind: "sso/bind/{phone}",//手机绑定
        signinWXUser: "sso/signinWXUser",//手机绑定
        getOpenid: "sso/getOpenid",//手机绑定
        signinWXUserUsingPOST: "https://mbeg168.cn/portal/swagger-ui.html#!/UmsMemberController/signinWXUserUsingPOST",//手机绑定
        getAuthCode: "sso/getAuthCode?telephone=",//获取验证码
        getExpiredDate: "sso/getExpiredDate",//获取当前Token过期的时间
        info: "sso/info",//获取当前登录用户信息
        login: "sso/login",//登录以后返回token
        loginByWX: "sso/loginByWX",//微信登录后返回token，如果没有电话，则新增NOTE返回标识
        merge: "sso/merge",//合并微信与系统原有账号
        refreshToken: "sso/refreshToken",//刷新Token
        register: "sso/register",//注册
        update: "sso/update",//更新当前登录用户信息
        updatePassword: "sso/updatePassword",//修改密码
    },
    common: {
        config: "config/getinfo", // 获取服务端配置
        update: "config/update", // 检测更新
        bindPhone: 'sso/bind/'
    },
    car: {
        addCar: "carbarn/addCar",//增加车辆信息到我的车库
        deleteCar: "carbarn/deleteCar",//从我的车库删除车辆
        queryMyCarlist: "carbarn/queryMyCarlist",//查询我的车库信息
        queryCarlist: "carbarn/queryCarlist",//查询车辆信息
        queryCarDetail: "carbarn/queryCarDetail",//查询车辆信息详情
    },
}
