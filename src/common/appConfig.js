"use strict";
////订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单
////0->待付款；1->待发货；2->已发货；3->已完成；4->订单正常已关闭；5->订单已取消
////6->申请退款 7->正在退款 8->退款成功 订单关闭 9->退款失败，请人工处理
///10->申请退货  11->退货申请接受 12->退货收到，正在退款 13 退货退款成功，订单关闭，
//14 退货退款失败，人工处理
////
exports.__esModule = true;
exports.ORDER_STATUS = exports.AJAX_STATUS = exports.ORDER_TYPE = void 0;
var ORDER_TYPE;
(function (ORDER_TYPE) {
    ORDER_TYPE[ORDER_TYPE["\u5F85\u4ED8\u6B3E"] = 0] = "\u5F85\u4ED8\u6B3E";
    ORDER_TYPE[ORDER_TYPE["\u5F85\u53D1\u8D27"] = 1] = "\u5F85\u53D1\u8D27";
    ORDER_TYPE[ORDER_TYPE["\u5F85\u6536\u8D27"] = 2] = "\u5F85\u6536\u8D27";
    ORDER_TYPE[ORDER_TYPE["\u5DF2\u6536\u8D27"] = 3] = "\u5DF2\u6536\u8D27";
    ORDER_TYPE[ORDER_TYPE["\u5F85\u9000\u6B3E"] = 4] = "\u5F85\u9000\u6B3E";
})(ORDER_TYPE = exports.ORDER_TYPE || (exports.ORDER_TYPE = {}));
var AJAX_STATUS;
(function (AJAX_STATUS) {
    AJAX_STATUS[AJAX_STATUS["ok"] = 200] = "ok";
    AJAX_STATUS[AJAX_STATUS["noLogin"] = 403] = "noLogin";
})(AJAX_STATUS = exports.AJAX_STATUS || (exports.AJAX_STATUS = {}));
////订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单
////0->待付款；1->待发货；2->已发货；3->已完成；4->订单正常已关闭；5->订单已取消
////6->申请退款 7->正在退款 8->退款成功 订单关闭 9->退款失败，请人工处理
///10->申请退货  11->退货申请接受 12->退货收到，正在退款 13 退货退款成功，订单关闭，
//14 退货退款失败，人工处理
////
exports.ORDER_STATUS = ['unpaid', 'back', 'unreceived', 'received', 'completed', 'cancelled', 'refunds'];
