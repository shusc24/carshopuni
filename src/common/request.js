import axios from 'axios';

const service = axios.create({
    // process.env.NODE_ENV === 'development' 来判断是否开发环境
    // easy-mock服务挂了，暂时不使用了
    baseURL: 'http://localhost/bank',
    timeout: 5000
});

service.interceptors.request.use(
	//此处进行token等数据处理
    config => {
		if (localStorage.getItem('token')) {
			config.headers.token = localStorage.getItem('token');
		}
        return config;
    },
	// Do something with request error
    error => {
        console.log(error);
        return Promise.reject();
    }
);

service.interceptors.response.use(
    response => {
        if (response.status === 200) {
            return response.data;
        } else {
            Promise.reject();
        }
    },
    error => {
        console.log(error);
        return Promise.reject();
    }
);

export default service;
