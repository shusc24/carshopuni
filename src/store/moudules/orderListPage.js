import {OrderApi} from "@/api/user";
import createLoadMoreStore from "@/store/components/loadMore";

let listStore = createLoadMoreStore();

export default  {
    namespaced: true,
    state:{
        orderType: ['待付款', '待发货', '待收货', '已收货', '待退款'],
        tabbarIndex: 0
    },
    mutations:{
        SET_TABLE_TAR_INDEX(state, index){
            state.tabbarIndex = index;
        }
    },
    actions:{
        async fetchList({dispatch, state, getters}, p = 1 ){
            return dispatch("list/fetchListData",{
                opt:{pageNum: p, pageSize: 10, status: getters.queryStatus},
                next: false,
                api: OrderApi.list
            })
        },
        async fetchNextData({dispatch, state, getters}){
            return dispatch("list/fetchListData",{
                opt:{pageSize: 5, status:　getters.queryStatus},
                next: true,
                api: OrderApi.list,
            })
        },
    },
    modules:{
        list:{
            namespaced: true,
            state: {...listStore.state},
            mutations: {...listStore.mutations},
            actions: {...listStore.actions}
        }
    },
    getters:{
        orderList(state){
            return state.list.items
        },
        queryStatus(state){
            if(state.tabbarIndex >= 4){
                return "4,6,7"
            }else {
                return state.tabbarIndex
            }
        }
    }
}
