import _ from "underscore"
import {AJAX_STATUS} from "@/common/appConfig";

export default function createLoadMoreStore (){
    return {
        namespaced: true,
        state : {
            pageSize: 5,
            total: 1,
            page: 1,
            loading: true,
            loadingState: false,
            finish: false,
            items: [],
        },
        mutations : {
            setLoadingState(state,val) {
                state.loadingState = val;
            },
            setItems(state,items) {
                state.items = items;
            },
            pushItem(state,items) {
                state.items = _.clone(state.items.concat(items));
            },
            setTotal(state,num) {
                state.total = num;
            },
            setPage(state,num) {
                state.page = num;
            },
            toggleListLoading(state,vis) {
                state.loading = vis;
            },
            toggleListFinish(state,vis) {
                state.finish = vis;
            }
        },
        actions : {
            fetchListData: async ({state,commit,dispatch},{opt, next, api}) => {
                try {
                    let {page, pageSize, loadingState} = state;
                    let Rs = {
                        status: -1,
                        data:"",
                        message:""
                    }
                    if (!loadingState) {
                        commit("toggleListLoading",true);
                        commit('setLoadingState', true);
                        let p = opt.pageNum ? opt.pageNum : page;

                        if (next) {
                            p++
                        }

                        Rs = await api(Object.assign({
                            pageNum: p, //当前页
                            pageSize: pageSize, //每页多少条数据
                        }, opt));
                        let { code, data} = Rs;
                        if (code === AJAX_STATUS.ok && Array.isArray(data.list)) {
                            let {pageNum, totalPage, list } = data;
                            //如果为1 则先清空数据
                            if (pageNum === 1) {
                                commit('setItems',[])
                            }

                            console.log(totalPage, pageNum);

                            commit("toggleListFinish", pageNum >= totalPage);
                            commit("toggleListLoading", pageNum < totalPage);
                            commit(next ? 'pushItem' : 'setItems', list);
                            commit("setPage", pageNum);
                            commit("setTotal", totalPage);
                        } else {
                            dispatch('setDefaultData');
                        }
                        commit("setLoadingState",false)
                    }
                    return Rs
                } catch (e) {

                }
            },
            setDefaultData({commit}){
                commit("toggleListLoading", false);
                commit("toggleListFinish", false);
                commit("setItems", []);
                commit("setPage", 1);
                commit("setTotal", 1);
            }
        }
    }
}
