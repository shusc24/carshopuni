import httpRequest from '@/common/httpRequest';
import * as httpApi from "@/common/httpApi";

let SESSION_ID = uni.getStorageSync("sessionid")
let AJAX_HEAD = uni.getStorageSync("hearder")

export function ReGetToken() {
    SESSION_ID = uni.getStorageSync("sessionid")
    AJAX_HEAD = uni.getStorageSync("hearder")
    return AJAX_HEAD + ' ' + SESSION_ID
}


export const OrderApi = {
    list(data = {}) {
        return httpRequest.get(httpApi.order.listByStatus, data, ReGetToken())
    },
    cancelOrder(data = {}) {
        return httpRequest.post(httpApi.order.cancelOrder + "?orderId=" + data.orderId, data, ReGetToken())
    },
    returnApplyCreate(data = {}) {
        return httpRequest.post(httpApi.returnApply.create, data, ReGetToken())
    },
    refirm(data = {}) {
        return httpRequest.post(httpApi.order.refirm + data.id, {}, ReGetToken())
    }
}

export const ShopApi = {
    getShopDetails(data = {}) {
        return httpRequest.get(httpApi.shop.getShopDetail + "?shop_id=" + data.id, data, ReGetToken())
    },
    getProductList(data = {}) {
        return httpRequest.get(httpApi.product.list, data, ReGetToken())
    }
}

export const CommonApi = {
    bindPhone(data) {
        return httpRequest.post(httpApi.common.bindPhone + data.phone, data, ReGetToken());
    }
}



