import request from '../../common/request.js';
import config from '../../common/config.js';

// 注册新用户
export const reg = params => {
    return request({
        url: ROOTPATH+'/sso/recommendUser',
        method: 'POST',
        params: params
    });
};
