import request from '../../common/request.js';

//获得当前用户的信息
export const msg = params => {
    return request({
        url: '/user/msg',
        method: 'POST',
        params: params
    });
};

// 手机号验证码登录
export const phonelogin = params => {
    return request({
        url: '/user/phonelogin',
        method: 'POST',
        params: params
    });
};

// 账户名密码或手机号密码登录
export const namelogin = params => {
    return request({
        url: '/user/namelogin',
        method: 'POST',
        params: params
    });
};