import request from '../../common/request.js';

// 注册新用户
export const modify = params => {
    return request({
        url: '/user/modify',
        method: 'POST',
        params: params
    });
};
